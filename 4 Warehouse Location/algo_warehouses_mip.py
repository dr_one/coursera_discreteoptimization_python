from gurobipy.gurobipy import *

WAREHOUSE = None
WAREHOUSE_COUNT = None
CUSTOMER_COST = None
CUSTOMER_COUNT = None
CUSTOMER_SIZE = None


def init(warehouses, customer_costs, customer_size):
    global WAREHOUSE, WAREHOUSE_COUNT, CUSTOMER_COST, CUSTOMER_COUNT, CUSTOMER_SIZE
    WAREHOUSE = warehouses
    WAREHOUSE_COUNT = len(WAREHOUSE)
    CUSTOMER_COST = customer_costs
    CUSTOMER_COUNT = len(CUSTOMER_COST)
    CUSTOMER_SIZE = customer_size


def solve_mip(threshold=0):

    def model_callback(model, where):
        if where == GRB.callback.MIP:
            best = model.cbGet(GRB.callback.MIP_OBJBST)
            print best
            if best <= threshold:
                model.terminate()
                print 'solution: ', best

    model = Model("warehouses")

    # Decision variables
    warehouses_open = []
    for w_i in xrange(WAREHOUSE_COUNT):
        warehouses_open.append(model.addVar(lb=0, ub=1, vtype=GRB.BINARY, name="w_%d" % w_i))
    customer_warehouse = []
    for c_i in xrange(CUSTOMER_COUNT):
        customer_warehouse.append([])
        for w_i in xrange(WAREHOUSE_COUNT):
            customer_warehouse[c_i].append(model.addVar(lb=0, ub=1, vtype=GRB.BINARY, name="w_%d_c_%d" % (w_i, c_i)))

    model.modelSense = GRB.MINIMIZE
    model.update()

    # Objective function
    objective_function = [WAREHOUSE[i][1] * warehouses_open[i] for i in range(WAREHOUSE_COUNT)]
    for c_i in xrange(CUSTOMER_COUNT):
        objective_function += [CUSTOMER_COST[c_i][w_i] * customer_warehouse[c_i][w_i] for w_i in range(WAREHOUSE_COUNT)]
    model.setObjective(quicksum(objective_function))

    # Constraints
    #model.addConstr(quicksum(warehouses_open), GRB.EQUAL, 100)
    #model.addConstr(quicksum(objective_function), GRB.LESS_EQUAL, 5284)

    for c_i in xrange(CUSTOMER_COUNT):
        model.addConstr(quicksum(customer_warehouse[c_i]), GRB.EQUAL, 1)
    model.optimize()
    print
    print 'adding constraints'
    print
    for w_i in xrange(WAREHOUSE_COUNT):
        for c_i in xrange(CUSTOMER_COUNT):
            model.addConstr(customer_warehouse[c_i][w_i], GRB.LESS_EQUAL, warehouses_open[w_i])
    for w_i in xrange(WAREHOUSE_COUNT):
        model.addConstr(quicksum([customer_warehouse[c_i][w_i] * CUSTOMER_SIZE[c_i] for c_i in xrange(CUSTOMER_COUNT)]), GRB.LESS_EQUAL, WAREHOUSE[w_i][0])

    model.update()
    model.optimize(model_callback)

    try:
        result = []
        for customer in customer_warehouse:
            result.append([round(item.x) for item in customer].index(1))
        return model.objVal, result
    except:
        print 'Model is infeasible'
        pass


    return None, None


def solve(warehouses, customer_costs, customer_size):
    init(warehouses, customer_costs, customer_size)
    value, result = solve_mip()
    return result







