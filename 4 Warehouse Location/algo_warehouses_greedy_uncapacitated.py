import itertools


WAREHOUSE = None
WAREHOUSE_COUNT = None
CUSTOMER_COST = None
CUSTOMER_COUNT = None
CUSTOMER_PREFERENCE = None
WAREHOUSE_MAX = 10


def init(customer_costs, warehouses):
    global WAREHOUSE, WAREHOUSE_COUNT, CUSTOMER_COST, CUSTOMER_COUNT, CUSTOMER_PREFERENCE
    WAREHOUSE = warehouses
    WAREHOUSE_COUNT = len(WAREHOUSE)
    CUSTOMER_COST = customer_costs
    CUSTOMER_COUNT = len(CUSTOMER_COST)

    CUSTOMER_PREFERENCE = []
    for i in range(CUSTOMER_COUNT):
        CUSTOMER_PREFERENCE.append([y for (x, y) in sorted(zip(CUSTOMER_COST[i], range(WAREHOUSE_COUNT)))])


def solve(warehouses, customer_costs):

    init(customer_costs, warehouses)

    bids = [sum(cost) for cost in zip(*CUSTOMER_COST)]
    bids = [x + y[0] for x, y in zip(bids, WAREHOUSE)]
    preferable_order = [y for (x, y) in sorted(zip(bids, range(WAREHOUSE_COUNT)), reverse=True)]

    best_solution, best_cost = None, float("inf")
    for cnt in range(1, WAREHOUSE_MAX):
        for combination in itertools.combinations(preferable_order[:WAREHOUSE_MAX], cnt):
            solution = attach_customers(combination)
            cost = calculate_cost(solution)
            if cost < best_cost:
                best_solution, best_cost = solution, cost
                print "Found: ", combination, best_cost, best_solution

    return best_solution


def attach_customers(opened_warehouse):
    res = [None] * CUSTOMER_COUNT
    for i in range(CUSTOMER_COUNT):
        for j in CUSTOMER_PREFERENCE[i]:
            if j in opened_warehouse:
                res[i] = j
                break
    return res


def calculate_cost(solution):
    used = [0] * WAREHOUSE_COUNT
    for wa in solution:
        used[wa] = 1
    obj = sum([WAREHOUSE[x][1] * used[x] for x in range(WAREHOUSE_COUNT)])
    for c in range(CUSTOMER_COUNT):
        obj += CUSTOMER_COST[c][solution[c]]
    return obj
