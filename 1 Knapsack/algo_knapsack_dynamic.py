def solve(n, k, weight, value):
    matrix = [[0 for i in range(n + 1)] for j in range(k + 1)]

    for j in range(1, k + 1):
        for i in range(1, n + 1):
            w_i = weight[i - 1]
            v_i = value[i - 1]
            if w_i <= j:
                matrix[j][i] = max(matrix[j][i - 1], matrix[j - w_i][i - 1] + v_i)
            else:
                matrix[j][i] = matrix[j][i - 1]

    items, j = [], k

    for i in range(n, 0, -1):
        is_selected = 0
        if matrix[j][i] != matrix[j][i - 1]:
            is_selected = 1
            j -= weight[i - 1]
        items.insert(0, is_selected)

    return matrix[k][n], 1, items