from collections import deque


def solve(n, k, weights, values):
    fractions = [1.0 * values[i] / weights[i] for i in range(n)]
    weights_sorted = [y for (x, y) in sorted(zip(fractions, weights), reverse=True)]
    values_sorted = [y for (x, y) in sorted(zip(fractions, values), reverse=True)]

    value, is_optimal, selected_items = solveBB(n, k, weights_sorted, values_sorted)

    items = [0] * n
    for item in selected_items:
        for i in range(n):
            if weights[i] == item[0] and values[i] == item[1]:
                items[i] = 1
                break

    return value, is_optimal, items


def solveBB(n, k, weights, values):

    min_weight = min(weights)
    fractions = [1.0 * values[i] / weights[i] for i in range(n)]

    def estimate(n, capacity, weights, values, from_index, min_weight, fractions):
        result = 0
        if from_index >= n or capacity < min_weight:
            return 0

        i = from_index
        for i in xrange(from_index, n):
            if capacity >= weights[i]:
                capacity -= weights[i]
                result += values[i]
            else:
                break

        result += capacity * fractions[i]

        return result

    best_value = 0
    best_items = []

    upper_bound = estimate(n, k, weights, values, 0, min_weight, fractions)

    start_node = [[0, k, upper_bound, [0] * n, -1]]

    stack = deque(start_node)
    cnt = 1
    skipped = 0

    while len(stack) > 0:
        node = stack.popleft()
        cnt += 1

        value = node[0]
        room = node[1]
        estimation = node[2]
        items = node[3]
        i = node[4] + 1

        assert estimation <= upper_bound

        if value > best_value:
            best_value = value
            best_items = list(items)

        if i >= n:
            continue

        # left node
        left_value = value + values[i]
        left_room = room - weights[i]
        left_items = list(items)
        left_items[i] = 1
        if left_room >= 0:
            stack.append([left_value, left_room, estimation, left_items, i])
        else:
            skipped += 1

        # right node
        right_estimation = estimate(n, room, weights, values, i + 1, min_weight, fractions)
        right_estimation += value
        if best_value < right_estimation:
            stack.append([value, room, right_estimation, items, i])
        else:
            skipped += 1

    result_items = []
    for i in range(n):
        if best_items[i] == 1:
            result_items.append((weights[i], values[i]))

    print 'Result Items', result_items
    print 'Weight: ', sum([pair[0] for pair in result_items])
    print 'Value: ', sum([pair[1] for pair in result_items])

    print 'Nodes checked: ', cnt
    print 'Skipped: ', skipped

    return best_value, 1, result_items