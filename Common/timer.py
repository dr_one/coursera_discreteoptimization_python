import time


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        res = method(*args, **kw)
        te = time.time()
        print "Elapsed time:", te - ts
        return res
    return timed