from collections import deque
from random import choice, randrange
from Common.timer import timeit


COUNT = None
TABU_LIST = deque()
TABU_LIST_CAPACITY = 10


ROWS = None
LEFT_DIAGONAL = None
RIGHT_DIAGONAL = None
QUEEN = None


left_diagonal_index = lambda row, column: row + column
right_diagonal_index = lambda row, column: (COUNT - 1) + row - column


def init(n):
    global COUNT, ROWS, LEFT_DIAGONAL, RIGHT_DIAGONAL, QUEEN
    COUNT = n
    ROWS = [0] * COUNT
    LEFT_DIAGONAL, RIGHT_DIAGONAL = [0] * (COUNT * 2 - 1), [0] * (COUNT * 2 - 1)
    QUEEN = [(i, None) for i in range(COUNT)]  # (column, row)
    for i in range(COUNT):
        add_queen(i, randrange(COUNT))


def add_queen(i, new_row):
    curr_column, curr_row = QUEEN[i]
    remove_queen(i)
    QUEEN[i] = (i, new_row)
    ROWS[new_row] += 1
    LEFT_DIAGONAL[left_diagonal_index(new_row, curr_column)] += 1
    RIGHT_DIAGONAL[right_diagonal_index(new_row, curr_column)] += 1


def remove_queen(i):
    if QUEEN[i] == (i, None):
        return
    curr_column, curr_row = QUEEN[i]
    QUEEN[i] = (i, None)
    ROWS[curr_row] -= 1
    LEFT_DIAGONAL[left_diagonal_index(curr_row, curr_column)] -= 1
    RIGHT_DIAGONAL[right_diagonal_index(curr_row, curr_column)] -= 1
    pass


def move_queen_on_best_position(i):
    remove_queen(i)
    best_row, best_value = None, float("inf")
    for row in range(COUNT):
        add_queen(i, row)
        if QUEEN[i] in TABU_LIST:
            continue
        if get_queen_violations(i) < best_value:
            best_row, best_value = row, get_queen_violations(i)
        remove_queen(i)
    assert best_row is not None
    add_queen(i, best_row)
    TABU_LIST.append(QUEEN[i])
    if len(TABU_LIST) >= TABU_LIST_CAPACITY:
        TABU_LIST.popleft()


def get_queen_violations(i):
    column, row = QUEEN[i]
    return (ROWS[row] - 1 +
            LEFT_DIAGONAL[left_diagonal_index(row, column)] - 1 +
            RIGHT_DIAGONAL[right_diagonal_index(row, column)] - 1)


def get_violations():
    return [get_queen_violations(i) for i in range(COUNT)]


@timeit
def solve(n):
    init(n)
    step = 0
    while True:
        step += 1
        violations = get_violations()
        if sum(violations) == 0:
            break
        print sum(violations)
        m = max(violations)
        index = choice([i for i, j in enumerate(violations) if j == m])
        move_queen_on_best_position(index)

    res = [row for column, row in QUEEN]
    print "Found solution", " in ", step, " steps"
    print
    print n
    print res


#solve(100)
#solve(32768)
#solve(60000)
#print QUEEN
#print ROWS
#print LEFT_DIAGONAL
#print RIGHT_DIAGONAL
