import math
import numpy
from gurobipy.gurobipy import *
from Common.timer import timeit


global NODES
global COSTS
global COUNT
global SOLUTION


#@timer.timeit
def init(points):

    def distance(point1, point2):
        return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)

    global COUNT, NODES, COSTS
    COUNT = len(points)
    NODES = set(range(0, COUNT))
    COSTS = numpy.zeros((COUNT, COUNT))
    for i in xrange(COUNT):
        for j in xrange(COUNT):
            COSTS[i][j] = distance(points[i], points[j])


#@timeit
def solve(points):
    init(points)

    #Find minimum sub tour and add lazy constraint to eliminate it
    def sub_tour_elimination(model, where):
        if where == GRB.callback.MIPSOL:
            solution = get_solution(model)
            tour = min_sub_tour(solution)
            if len(tour) < COUNT:
                sub_tour_edges = []
                for item in [(tour[i], tour[i + 1]) for i in range(len(tour) - 1)] + [(tour[0], tour[-1])]:
                    sub_tour_edges.append((item[0], item[1]) if item[0] < item[1] else (item[1], item[0]))
                sub_tour_constraint = [model._vars[edge] for edge in sub_tour_edges]
                model.cbLazy(quicksum(sub_tour_constraint) <= len(sub_tour_edges)-1)
            else:
                print 'Solution: ', tour
                global SOLUTION
                SOLUTION = tour

    def get_solution(model):
        solution = []
        for key in model._vars:
            value = model.cbGetSolution(model._vars[key])
            if round(value) == 1.0:
                solution.append(key)
        return solution

    def min_sub_tour(solution):
        tours = []
        while len(solution) > 0:
            tours.append(all_sub_tour(solution))
        lengths = [len(tour) for tour in tours]
        return tours[lengths.index(min(lengths))]

    #might be optimized
    #@timeit
    def all_sub_tour(solution):
        tour = []
        curr = solution.pop()[0]
        while True:
            tour.append(curr)
            temp, index = [item for item in solution if item[0] == curr], 1
            if len(temp) == 0:
                temp, index = [item for item in solution if item[1] == curr], 0
            if len(temp) == 0:
                break
            curr, item = temp[0][index], temp[0]
            solution.remove(item)
        return tour

    model = Model("TSP")

    # Decision variables
    variables = {}
    for i in xrange(COUNT):
        for j in xrange(i):
            variables[j, i] = model.addVar(lb=0, ub=1, vtype=GRB.BINARY, name="%d_%d" % (j, i))

    model.update()

    # Objective function
    objective_function = [COSTS[key[0]][key[1]] * variables[key] for key in variables]
    model.setObjective(quicksum(objective_function))

    # Constraints
    for i in xrange(COUNT):
        model.addConstr(quicksum([variables[key] for key in variables if key[0] == i or key[1] == i]) == 2)

    # Optimization
    model._vars = variables
    model.setParam('NodefileStart', 0.5)
    model.setParam('Threads', 1)
    model.modelSense = GRB.MINIMIZE
    model.optimize(sub_tour_elimination)

    # Result
    print SOLUTION
    print len(SOLUTION)
    return SOLUTION



