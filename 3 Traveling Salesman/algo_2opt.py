import itertools
import math

import algo_nearest

global NODES
global COSTS
global COUNT


def solve(points):
    pre_process(points)
    start_path = algo_nearest.solve(points, 0)
    return apply_2opt(start_path)


def distance(point1, point2):
    return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)


#@timeit
def pre_process(points):
    global COUNT, NODES
    COUNT = len(points)
    NODES = set(range(0, COUNT))

#@timeit
def apply_2opt(path, points):
    curr_path = list(path)
    changed = True
    while changed:
        changed = False
        for (i1, j1) in itertools.combinations(range(COUNT), 2):
            if i1 == 0 or j1 - i1 < 2:
                continue
            i2 = i1 + 1
            j2 = (j1 + 1) % COUNT

            if distance(points[curr_path[i1]], points[curr_path[i2]]) + \
               distance(points[curr_path[j1]], points[curr_path[j2]]) > \
               distance(points[curr_path[i1]], points[curr_path[j1]]) + \
               distance(points[curr_path[i2]], points[curr_path[j2]]):
                temp = curr_path[i2:j1 + 1]
                curr_path[i2:j1 + 1] = temp[::-1]
                changed = True
                print 'improved', (i1, j1)
    return curr_path


