import math


def distance(point1, point2):
    return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)


def solve(points, start_node=0):
    node_count = len(points)
    solution = []
    visited = set()

    curr_node = start_node
    while len(visited) < node_count:
        visited.add(curr_node)
        solution.append(curr_node)

        neighbors_length = []
        for i in range(node_count):
            length = float('inf') if i in visited else distance(points[curr_node], points[i])
            neighbors_length.append(length)

        curr_node = neighbors_length.index(min(neighbors_length))

        print node_count - len(visited)

    return solution