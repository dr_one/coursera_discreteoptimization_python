import math
import random
import itertools
import numpy
import algo_nearest

global NODES
global COSTS
global COUNT


def calculate_cost(path):
    res = COSTS[path[-1], path[0]]
    for index in range(0, COUNT - 1):
        res += COSTS[path[index], path[index + 1]]
    return res


#@timer.timeit
def pre_process(points):

    def distance(point1, point2):
        return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)

    global COUNT, NODES, COSTS
    COUNT = len(points)
    NODES = set(range(0, COUNT))
    COSTS = numpy.zeros((COUNT, COUNT))
    for i in range(COUNT):
        for j in range(COUNT):
            COSTS[i][j] = distance(points[i], points[j])


def solve(points):
    pre_process(points)
    return solve_internal(points)


def solve_internal(points):

    curr_path = algo_nearest.solve(points, 0)

    iteration = -1
    temperature = 1000.0
    coolingRate = 0.99
    absoluteTemperature = 0.0001

    curr_cost = calculate_cost(curr_path)

    best_path = None
    best_cost = float("inf")

    while temperature > absoluteTemperature:

        #if curr_cost <= 70556547 and curr_cost != best_cost:
        #    print "Found :", curr_cost, curr_path
        if curr_cost <= 428.9:
            print "Best :", curr_cost, curr_path
            return curr_path

        print curr_cost

        index1, index2 = random.sample(curr_path, 2)
        new_path = list(curr_path)
        new_path[index1], new_path[index2] = new_path[index2], new_path[index1]
        new_path = apply_2opt(new_path)

        delta = calculate_cost(new_path) - curr_cost

        if delta < 0 or (delta > 0 and math.exp(- delta * 1.0 / temperature) > random.random()):
            curr_path = new_path
            curr_cost += delta
            if curr_cost < best_cost:
                best_path = curr_path

        temperature *= coolingRate

        if temperature < 0.0001:
            temperature = 1000
            print "Restarted", best_path

        iteration += 1

    #print curr_path, curr_cost
    return best_path


#@timer.timeit
def apply_2opt(path):
    curr_path = list(path)
    changed = True
    while changed:
        changed = False
        for (i1, j1) in itertools.combinations(range(COUNT), 2):
            if i1 == 0 or j1 - i1 < 2:
                continue
            i2 = i1 + 1
            j2 = (j1 + 1) % COUNT
            if COSTS[curr_path[i1]][curr_path[i2]] + COSTS[curr_path[j1]][curr_path[j2]] > \
                            COSTS[curr_path[i1]][curr_path[j1]] + COSTS[curr_path[i2]][curr_path[j2]]:
                temp = curr_path[i2:j1 + 1]
                curr_path[i2:j1+1] = temp[::-1]
                changed = True
    return curr_path

