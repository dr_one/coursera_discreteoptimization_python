#!/usr/bin/env python

import sys
import algo_nearest
import algo_simulated_annealing
import algo_2opt


def read_graph(stream):
    n = int(stream.readline())
    for ln in stream:
        yield map(float, ln.split())


def plot_graph(points, walk):

    print "graph G {\n  node [shape=point, fixedsize=true];"
    for (i, (x, y)) in enumerate(points, 1):
        print '  %d [label="", pos="%d,%d!"];' % (i, x, y)
    prev = walk[-1]
    for i in walk:
        print "  %d -- %d;" % (prev + 1, i + 1)
        prev = i
    print "}"


def main(alg_name, fname):
    with file(fname) as stream:
        func = get_algorithm(alg_name)
        points = list(read_graph(stream))
        plot_graph(points, func(points))


def get_algorithm(name):
    if name == "nearest":
        return algo_nearest.solve
    if name == "2opt":
        return algo_2opt.solve
    if name == "sa":
        return algo_simulated_annealing.solve


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])