#!/usr/bin/python
# -*- coding: utf-8 -*-

import math
import algo_traveling_salesman_mip

def length(point1, point2):
    return math.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)

def solveIt(inputData):
    # Modify this code to run your optimization algorithm

    # parse the input
    lines = inputData.split('\n')

    nodeCount = int(lines[0])

    points = []
    for i in range(1, nodeCount+1):
        line = lines[i]
        parts = line.split()
        points.append((float(parts[0]), float(parts[1])))

    # build a trivial solution
    # visit the nodes in the order they appear in the file
    solution = range(0, nodeCount)

    solution = algo_traveling_salesman_mip.solve(points)
    #solution = [10, 9, 45, 3, 27, 41, 24, 46, 8, 4, 34, 23, 35, 13, 7, 19, 40, 18, 16, 44, 14, 15, 38, 50, 39, 49, 17, 32, 48, 22, 31, 1, 25, 20, 37, 21, 43, 29, 42, 11, 30, 12, 36, 6, 26, 47, 33, 0, 5, 2, 28]
    #solution = [25, 19, 72, 70, 38, 90, 8, 22, 52, 18, 9, 53, 42, 48, 68, 41, 59, 63, 85, 6, 23, 49, 82, 94, 30, 46, 45, 36, 1, 60, 33, 97, 15, 93, 12, 0, 65, 86, 58, 27, 31, 56, 75, 10, 81, 73, 95, 78, 67, 98, 61, 89, 2, 50, 34, 76, 64, 62, 28, 13, 69, 91, 4, 16, 96, 80, 14, 29, 26, 79, 17, 84, 51, 3, 55, 24, 71, 57, 66, 74, 39, 83, 7, 47, 37, 77, 88, 87, 20, 5, 92, 54, 35, 21, 32, 11, 99, 44, 43, 40]
    #solution = [1, 143, 47, 140, 91, 116, 177, 54, 112, 86, 25, 162, 130, 147, 94, 55, 150, 27, 11, 114, 105, 103, 182, 81, 132, 46, 20, 181, 163, 113, 24, 19, 141, 9, 8, 101, 115, 4, 176, 2, 82, 39, 5, 17, 84, 58, 149, 63, 142, 188, 95, 85, 159, 173, 64, 186, 13, 67, 32, 165, 44, 98, 77, 30, 56, 71, 134, 160, 126, 75, 79, 193, 156, 133, 108, 124, 145, 45, 120, 189, 100, 194, 197, 73, 111, 60, 170, 6, 131, 66, 74, 158, 35, 128, 107, 198, 175, 196, 190, 28, 127, 57, 102, 110, 192, 21, 184, 172, 41, 22, 109, 167, 10, 88, 89, 16, 139, 138, 69, 152, 48, 169, 97, 166, 96, 104, 31, 93, 161, 125, 199, 155, 0, 49, 168, 174, 129, 80, 33, 148, 185, 37, 65, 7, 51, 137, 119, 179, 26, 23, 164, 87, 178, 12, 180, 78, 146, 40, 83, 136, 171, 68, 106, 183, 157, 151, 15, 62, 153, 14, 72, 38, 90, 53, 76, 42, 70, 187, 122, 121, 92, 3, 154, 43, 59, 52, 123, 117, 144, 135, 18, 191, 50, 118, 36, 195, 61, 34, 29, 99]



    # calculate the length of the tour
    obj = length(points[solution[-1]], points[solution[0]])
    for index in range(0, nodeCount-1):
        obj += length(points[solution[index]], points[solution[index+1]])

    # prepare the solution in the specified output format
    outputData = str(obj) + ' ' + str(0) + '\n'
    outputData += ' '.join(map(str, solution))

    return outputData


import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        fileLocation = sys.argv[1].strip()
        inputDataFile = open(fileLocation, 'r')
        inputData = ''.join(inputDataFile.readlines())
        inputDataFile.close()
        print solveIt(inputData)
    else:
        print 'This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/tsp_51_1)'

