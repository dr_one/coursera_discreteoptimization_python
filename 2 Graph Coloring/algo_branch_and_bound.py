from collections import deque
import copy

from Common.timer import timeit


global COLORS, GRAPH, DEGREE, STATS


@timeit
def solve(nodeCount, edges):

    print "Node count: ", nodeCount
    print "Edge count: ", len(edges)
    print "-------"

    global COLORS, GRAPH, DEGREE, STATS
    COLORS = [i for i in range(nodeCount)]
    GRAPH, DEGREE = build_graph(nodeCount, edges)
    STATS = stats()

    print DEGREE

    init_search_node = SearchNode()
    init_search_node.visited = [None] * nodeCount
    init_search_node.disabled_colors = [set() for i in range(nodeCount)]

    best_node = None
    best_colors_cnt = nodeCount

    bag = deque([init_search_node])

    while len(bag) > 0:
        curr_search_node = bag.popleft()
        curr_colors_cnt = len(curr_search_node.get_colors())
        STATS.search_node_cnt_add()

        # optimization 1: prune solution if it worse than already founded
        if best_node is not None and curr_colors_cnt >= best_colors_cnt:
            STATS.skipped_cnt_add()
            continue

        result = try_solve_node(curr_search_node)
        if result == False:
            continue

        if curr_search_node.is_solved():
            STATS.solution_cnt_add()
            if None == best_node or curr_colors_cnt < best_colors_cnt:
                best_node = SearchNode(curr_search_node)
                best_colors_cnt = len(best_node.get_colors())
                print "Found solution with ", best_colors_cnt, " colors"
                print ' '.join(map(str, curr_search_node.visited))
                #optimization 4: decrease amount of colors if solution was found
                COLORS = COLORS[:best_colors_cnt - 1]
                print "Available Colors: ", COLORS
            continue

        #optimization 2.a: firstly get elements with high degree
        temp1 = [DEGREE[i] if curr_search_node.visited[i] is None else None for i in range(nodeCount)]
        #optimization 2.b: firstly get elements with min possible options
        temp2 = [len(curr_search_node.disabled_colors[i]) if curr_search_node.visited[i] is None else None for i in range(nodeCount)]

        temp = []
        for i in range(nodeCount):
            sum = 0
            if temp1[i] is not None:
                sum += temp1[i]
            if temp2[i] is not None:
                sum += temp2[i]
            temp.append(sum)

        index = temp.index(max(temp))

        assert index != -1

        for new in curr_search_node.get_neighbors(index):
            bag.appendleft(new)

    print "-------"
    test_solution(GRAPH, best_node.visited)
    print "SOLUTION: ", best_node.visited
    print "-------"
    print "Solution count: ", STATS.solution_cnt
    print "Skipped nodes count: ", STATS.skipped_cnt
    print "Checked nodes count: ", STATS.search_node_cnt
    print "Fixed nodes count: ", STATS.fixed_cnt

    return best_node.visited


class SearchNode:
    visited = []
    disabled_colors = []

    def heuristic(self):
        pass

    def is_solved(self):
        return None not in self.visited

    def get_colors(self):
        res = set(self.visited)
        if None in res:
            res.remove(None)
        return res

    def get_neighbors(self, index):
        res = []
        #optimization 3: check only n + 1 colors for n-th node
        available_colors = [i for i in COLORS[: (len(self.get_colors())) + 1] if i not in self.disabled_colors[index]]
        for color in reversed(available_colors):
            new_search_node = SearchNode(self)
            new_search_node.visited[index] = color
            new_search_node.disabled_colors[index] = None
            for adj in GRAPH[index]:
                if new_search_node.visited[adj] is not None:
                    continue
                new_search_node.disabled_colors[adj].add(color)
            res.append(new_search_node)
        return res

    def __init__(self, other=None):
        if None != other:
            self.disabled_colors = copy.deepcopy(other.disabled_colors)
            self.visited = copy.copy(other.visited)

    def __str__(self):
        return str(self.visited) + "  " + str(self.disabled_colors)


def build_graph(nodeCount, edges):
    graph = {}
    degrees = []
    for i in range(nodeCount):
        graph[i] = []
    for edge in edges:
        graph[edge[0]].append(edge[1])
        graph[edge[1]].append(edge[0])
    for i in graph:
        degrees.append(len(graph[i]))
    return graph, degrees


def try_solve_node(search_node):
    colors = set(COLORS)
    colors_cnt = len(colors)
    while True:
        changed = False
        for i in range(len(search_node.visited)):
            if search_node.visited[i] is not None:
                continue

            assert search_node.disabled_colors[i] is not None

            disabled_colors_cnt = len(search_node.disabled_colors[i])
            if disabled_colors_cnt == colors_cnt:
                STATS.skipped_cnt_add()
                return False

            if disabled_colors_cnt + 1 == colors_cnt:
                color = (colors - search_node.disabled_colors[i]).pop()
                #check adjacent nodes
                for adj in GRAPH[i]:
                    if search_node.visited[adj] == color:
                        STATS.skipped_cnt_add()
                        return False #node is infeasible
                changed = True
                STATS.fixed_cnt_add()
                search_node.visited[i] = color
                search_node.disabled_colors[i] = None
                for adj in GRAPH[i]:
                    if search_node.visited[adj] is not None:
                        continue
                    search_node.disabled_colors[adj].add(color)

        if not changed:
            return


def test_solution(graph, solution):
    for i in graph:
        for adj in graph[i]:
            if solution[i] == solution[adj]:
                print 'ERROR'
                return False
    print 'SOLVED'
    return True


class stats:
    search_node_cnt = 0
    solution_cnt = 0
    skipped_cnt = 0
    fixed_cnt = 0

    def search_node_cnt_add(self):
        self.search_node_cnt += 1
        if self.search_node_cnt % 10000 == 0:
            print "Checked nodes: ", self.search_node_cnt

    def solution_cnt_add(self):
        self.solution_cnt += 1
        if self.solution_cnt % 10000 == 0:
            print "Fount solution: ", self.solution_cnt

    def skipped_cnt_add(self):
        self.skipped_cnt += 1
        if self.skipped_cnt % 10000 == 0:
            print "Skipped nodes: ", self.skipped_cnt

    def fixed_cnt_add(self):
        self.fixed_cnt += 1
        if self.fixed_cnt % 10000 == 0:
            print "Fixed nodes: ", self.fixed_cnt
