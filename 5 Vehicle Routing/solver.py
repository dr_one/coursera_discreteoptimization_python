#!/usr/bin/python
# -*- coding: utf-8 -*-

import math
import algo_vrp_simulated_annealing


def length(customer1, customer2):
    return math.sqrt((customer1[1] - customer2[1])**2 + (customer1[2] - customer2[2])**2)


def solveIt(inputData):
    # Modify this code to run your optimization algorithm

    # parse the input
    lines = inputData.split('\n')

    parts = lines[0].split()
    customerCount = int(parts[0])
    vehicleCount = int(parts[1])
    vehicleCapacity = int(parts[2])
    depotIndex = 0

    customers = []
    for i in range(1, customerCount+1):
        line = lines[i]
        parts = line.split()
        customers.append((int(parts[0]), float(parts[1]),float(parts[2])))

    # build a trivial solution
    # assign customers to vehicles starting by the largest customer demands

    vehicleTours = []

    customerIndexs = set(range(1, customerCount))  # start at 1 to remove depot index
    for v in range(0, vehicleCount):
        # print "Start Vehicle: ",v
        vehicleTours.append([])
        capacityRemaining = vehicleCapacity
        while sum([capacityRemaining >= customers[ci][0] for ci in customerIndexs]) > 0:
            used = set()
            order = (sorted(customerIndexs, key=lambda ci: -customers[ci][0]))
            for ci in order:
                if capacityRemaining >= customers[ci][0]:
                    capacityRemaining -= customers[ci][0]
                    vehicleTours[v].append(ci)
                    # print '   add', ci, capacityRemaining
                    used.add(ci)
            customerIndexs -= used

    # checks that the number of customers served is correct
    assert sum([len(v) for v in vehicleTours]) == customerCount - 1

    test = algo_vrp_simulated_annealing.solve(customerCount, customers, vehicleCount, vehicleCapacity, vehicleTours)
    print
    print
    print
    print

    #1)o 278.726299748 [[11, 2, 9, 12], [6, 7, 8, 3, 1], [14, 13, 4, 15, 10, 5]]
    #2)o 607.650945635 [[15, 19], [2, 4, 25, 10], [17, 5, 7], [24, 18, 22], [20, 21, 13, 16], [12, 11, 8, 9], [3], [14, 6, 1, 23]]
    #3)n [[47, 4, 42, 19, 40, 41, 13, 18], [32, 2, 16, 50, 9, 34, 30, 38, 11], [5, 49, 10, 39, 33, 45, 15, 44, 37, 17, 12], [27, 48, 23, 7, 43, 24, 25, 14, 6], [46, 1, 22, 8, 26, 31, 28, 3, 36, 35, 20, 29, 21]]
    #4)n 823.809633802 [[69, 68, 64, 61, 72, 80, 79, 77, 73, 70, 71, 76, 78, 81], [66, 62, 74, 63, 65, 67], [90, 87, 86, 83, 82, 84, 85, 88, 89, 91], [13, 17, 18, 19, 15, 16, 14, 12], [20, 24, 25, 27, 29, 30, 28, 26, 23, 22, 21], [31, 35, 37, 38, 39, 36, 34, 33, 32], [43, 42, 41, 40, 44, 46, 45, 48, 51, 50, 52, 49, 47], [10, 11, 9, 8, 6, 7, 5, 3, 4, 2, 1, 75], [99, 100, 97, 93, 92, 94, 95, 96, 98], [57, 59, 60, 58, 56, 53, 54, 55]]
    #5)n 3352.29894427 [[18, 96, 95, 117, 73, 197, 110, 25, 55, 130, 77, 136, 160, 90, 19, 132, 27], [111, 158, 179, 186, 21, 44, 118, 175, 64, 20, 31, 52], [149, 67, 170, 184, 3, 128, 32, 162, 193, 57, 13], [26, 74, 119, 38, 45, 199, 60, 166, 99, 53, 154, 129, 71, 9, 157], [88, 7, 6, 172, 42, 43, 139, 176, 70, 108, 30, 185, 169, 163, 12, 105], [40, 187, 102, 33, 66, 122, 86, 16, 92, 59, 156], [101, 11, 63, 51, 29, 134, 109, 195, 180, 152, 137, 98, 14, 173], [81, 120, 1, 69, 62, 106, 89, 151, 37, 97, 2, 145, 72], [146, 47, 36, 143, 159, 50, 76, 54, 198, 87, 191, 58], [94, 142, 178, 22, 155, 165, 103, 168, 5, 112], [133, 75, 56, 4, 24, 121, 79, 189, 107, 182, 82, 114], [144, 141, 84, 48, 194, 148, 131, 65, 190, 127], [28, 138, 116, 80, 34, 35, 188, 181, 49, 8, 153, 183], [167, 174, 104, 61, 140, 15, 41, 23, 68, 135, 196], [147, 93, 91, 192, 115, 39, 177, 161, 126, 123, 124, 46, 83], [171, 100, 85, 113, 17, 125, 10, 164, 78, 150]]


    #vehicleTours = [[105, 145, 144, 124, 63, 90, 160, 71, 65, 35, 169, 102, 162], [89, 83, 113, 85, 193, 2, 133, 110, 116, 136, 66, 1], [156, 112, 115, 59, 44, 174, 82, 120, 129, 184], [109, 79, 135, 131, 10, 168, 98, 151, 92, 22, 56, 198, 40, 152], [127, 88, 49, 188, 50, 68, 155, 23, 13], [130, 158, 51, 159, 18, 118, 45, 17, 86, 140, 172], [121, 55, 170, 75, 142, 141, 91, 95, 175, 30, 161, 122, 101], [197, 74, 41, 57, 43, 192, 99, 96, 183, 27, 132, 77, 20, 107, 7, 166], [28, 138, 171, 163, 3, 32, 126, 36, 47, 125, 5], [69, 81, 108, 11, 64, 48, 8, 173, 178, 180, 195], [53, 21, 54, 134, 24, 80, 103, 128, 123, 182, 104, 100, 37, 137], [179, 4, 165, 34, 33, 185, 196, 76, 52, 19, 143, 46, 61, 38, 58], [93, 97, 15, 67, 187, 177, 78, 164, 181, 189, 31], [94, 117, 87, 42, 14, 119, 16, 84, 199, 114, 106, 148, 167, 146], [153, 194, 190, 111, 12, 150, 29, 25, 39, 73, 149, 26], [176, 157, 9, 70, 62, 60, 191, 6, 147, 72, 186, 139, 154]]



    # calculate the cost of the solution; for each vehicle the length of the route
    obj = 0
    for v in range(0, vehicleCount):
        vehicleTour = vehicleTours[v]
        if len(vehicleTour) > 0:
            obj += length(customers[depotIndex],customers[vehicleTour[0]])
            for i in range(0, len(vehicleTour) - 1):
                obj += length(customers[vehicleTour[i]],customers[vehicleTour[i + 1]])
            obj += length(customers[vehicleTour[-1]],customers[depotIndex])

    # prepare the solution in the specified output format
    outputData = str(obj) + ' ' + str(0) + '\n'
    for v in range(0, vehicleCount):
        outputData += str(depotIndex) + ' ' + ' '.join(map(str,vehicleTours[v])) + ' ' + str(depotIndex) + '\n'

    return outputData


import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        fileLocation = sys.argv[1].strip()
        inputDataFile = open(fileLocation, 'r')
        inputData = ''.join(inputDataFile.readlines())
        inputDataFile.close()
        print 'Solving:', fileLocation
        print solveIt(inputData)
    else:

        print 'This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/vrp_5_4_1)'

