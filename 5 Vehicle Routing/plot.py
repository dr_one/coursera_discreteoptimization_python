import matplotlib.pyplot as plt
import numpy
import math
import sys

problem_file_name  = sys.argv[1]
solution_file_name = sys.argv[2]

tours = []
with open( solution_file_name, 'r' ) as sol_file:
    sol_file.readline()
    for l in sol_file:
        tour = l.split()
        if len( tour ) > 0 :
            tours.append( map( int, tour ) )

customers = []
with open( problem_file_name, 'r' ) as prob_file:
    prob_file.readline()
    for l in prob_file:
        c = l.split()
        if len( c ) > 0 :
            customers.append( ( c[1],c[2] ) )

customers = numpy.array( customers )
for t in tours:
    nn = customers[t]
    plt.plot(nn[:,0],nn[:,1])
plt.scatter(customers[:,0], customers[:,1],s=50)
plt.show()