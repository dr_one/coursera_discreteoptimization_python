import copy
import math
import random
import numpy

global VEHICLE_COUNT
global VEHICLE_CAPACITY
global CUSTOMER_COUNT
global CUSTOMERS
global DEPOT_INDEX
global COSTS
global PENALTY_FACTOR


def solve(customer_count, customers, vehicle_count, vehicle_capacity, feasible_solution):

    def distance(point1, point2):
        return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)

    global VEHICLE_COUNT, VEHICLE_CAPACITY, CUSTOMER_COUNT, CUSTOMERS, DEPOT_INDEX, COSTS, PENALTY_FACTOR
    VEHICLE_COUNT = vehicle_count
    VEHICLE_CAPACITY = vehicle_capacity
    CUSTOMER_COUNT = customer_count
    CUSTOMERS = customers
    DEPOT_INDEX = 0
    PENALTY_FACTOR = 10

    print VEHICLE_COUNT, VEHICLE_CAPACITY, CUSTOMER_COUNT

    COSTS = numpy.zeros((CUSTOMER_COUNT, CUSTOMER_COUNT))
    for i in xrange(CUSTOMER_COUNT):
        for j in xrange(CUSTOMER_COUNT):
            COSTS[i][j] = distance((CUSTOMERS[i][1], CUSTOMERS[i][2]), (CUSTOMERS[j][1], CUSTOMERS[j][2]))

    return solve_internal(feasible_solution)


def solve_internal(initial_solution):

    print 'Start Simulated Annealing'

    iteration = -1
    temperature = 1000.0
    coolingRate = 0.9999
    absoluteTemperature = 0.0001

    initial_solution = [[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 60, 61, 62, 63], [64, 65, 66, 67, 68], [69, 70, 71, 120, 121], [122, 123, 124, 125, 126], [127, 128, 129, 130, 131], [180, 181, 182, 183, 184], [185, 186, 187, 188, 189], [190, 191, 240, 241, 242], [243, 244, 245, 246, 247], [248, 249, 250, 251, 300], [301, 302, 303, 304, 305], [306, 307, 308, 309, 310], [311, 360, 361, 362, 363], [364, 365, 366, 367, 368], [369, 370, 371, 420, 15, 18], [23, 26, 31, 34, 39, 42, 47, 50, 55, 58], [75, 78, 83, 86, 91, 94, 99, 102, 107, 110], [115, 118, 135, 138, 143, 146, 151, 154, 159, 162], [167, 170, 175, 178, 195, 198, 203, 206, 211, 214], [219, 222, 227, 230, 235, 238, 255, 258, 263, 266], [271, 274, 279, 282, 287, 290, 295, 298, 315, 318], [323, 326, 331, 334, 339, 342, 347, 350, 355, 358], [375, 378, 383, 386, 391, 394, 399, 402, 407, 410], [415, 418, 12, 13, 14, 16, 17, 19, 20, 21, 22, 24, 25, 27, 28, 29, 30, 32], [33, 35, 36, 37, 38, 40, 41, 43, 44, 45, 46, 48, 49, 51, 52, 53, 54, 56, 57, 59], [72, 73, 74, 76, 77, 79, 80, 81, 82, 84, 85, 87, 88, 89, 90, 92, 93, 95, 96, 97], [98, 100, 101, 103, 104, 105, 106, 108, 109, 111, 112, 113, 114, 116, 117, 119, 132, 133, 134, 136], [137, 139, 140, 141, 142, 144, 145, 147, 148, 149, 150, 152, 153, 155, 156, 157, 158, 160, 161, 163], [164, 165, 166, 168, 169, 171, 172, 173, 174, 176, 177, 179, 192, 193, 194, 196, 197, 199, 200, 201], [202, 204, 205, 207, 208, 209, 210, 212, 213, 215, 216, 217, 218, 220, 221, 223, 224, 225, 226, 228], [229, 231, 232, 233, 234, 236, 237, 239, 252, 253, 254, 256, 257, 259, 260, 261, 262, 264, 265, 267], [268, 269, 270, 272, 273, 275, 276, 277, 278, 280, 281, 283, 284, 285, 286, 288, 289, 291, 292, 293], [294, 296, 297, 299, 312, 313, 314, 316, 317, 319, 320, 321, 322, 324, 325, 327, 328, 329, 330, 332], [333, 335, 336, 337, 338, 340, 341, 343, 344, 345, 346, 348, 349, 351, 352, 353, 354, 356, 357, 359], [372, 373, 374, 376, 377, 379, 380, 381, 382, 384, 385, 387, 388, 389, 390, 392, 393, 395, 396, 397], [398, 400, 401, 403, 404, 405, 406, 408, 409, 411, 412, 413, 414, 419], [416], [417], [419]]
    best_solution = curr_solution = copy.deepcopy(initial_solution)
    best_cost = curr_cost = calculate_cost(curr_solution)

    print "Initial solution: ", curr_cost, curr_solution
    assert calculate_penalty(curr_solution) == 0

    while temperature > absoluteTemperature:

        new_solution = copy.deepcopy(curr_solution)
        swap(new_solution)

        new_cost = calculate_cost(new_solution)
        penalty = calculate_penalty(new_solution)

        delta = new_cost - curr_cost + penalty
        if delta < 0 or (delta > 0 and math.exp(- delta * 1.0 / temperature) > random.random()):

            curr_solution = new_solution
            curr_cost += delta

            if curr_cost < best_cost and penalty == 0:
                best_solution = curr_solution
                best_cost = curr_cost
                print best_cost, best_solution

                if best_cost <= 2392:
                    print "Got 7 points! ", best_cost, best_solution

                if best_cost <= 1829.8:
                    print "Best :", best_cost, best_solution
                    return

        temperature *= coolingRate
        if temperature < absoluteTemperature:
            temperature = 1000.0
            print "Restarted", best_cost, best_solution

        iteration += 1

    return best_solution


def swap(solution):
    vehicle_tour_from = random.choice(range(VEHICLE_COUNT))
    vehicle_tour_to = random.choice(range(VEHICLE_COUNT))
    #at least one customer per vehicle
    if len(solution[vehicle_tour_from]) <= 1:
        return
    index_from = random.choice(range(len(solution[vehicle_tour_from])))
    index_to = random.choice(range(len(solution[vehicle_tour_to])))
    customer = solution[vehicle_tour_from].pop(index_from)
    solution[vehicle_tour_to].insert(index_to, customer)


def calculate_penalty(solution):
    res = 0
    for vehicleTour in solution:
        capacity = sum([CUSTOMERS[i][0] for i in vehicleTour])
        if capacity > VEHICLE_CAPACITY:
            res += (capacity - VEHICLE_CAPACITY)
    return res * PENALTY_FACTOR


def calculate_cost(solution):
    res = 0
    for vehicleTour in solution:
        if len(vehicleTour) > 0:
            res += COSTS[DEPOT_INDEX][vehicleTour[0]]
            for i in xrange(len(vehicleTour) - 1):
                res += COSTS[vehicleTour[i]][vehicleTour[i + 1]]
            res += COSTS[vehicleTour[-1]][DEPOT_INDEX]
    return res
